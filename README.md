# Design Patterns OOP with Java
## _The Factory Method_

This project implements a simplified example for design pattern  Factory Method  

## Problem

Consider that you need an instance of class Database, but in sometime you change of database supplier of according with your client.  The problem is: if client is NSA then use Oracle, else use Postgres.

_How resolve this?_ 

The factory method is an option to resolve this problem. You can change database whithout alter your classes or create specifc classes to client.


## Example

```java

        Database database = new DatabaseFactory().getDatabase(); // get default database oracle;
        database.connect();
        database.find(1);
        database.update(1, "name"));

        database = new DatabaseFactory().getDatabase(POSTGRE_SQL); // is possible load by env;
        database.find(1);
        database.update(1, "name");
```
