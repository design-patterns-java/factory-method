package com.jonasrodrigues;

import com.jonasrodrigues.databases.Database;
import com.jonasrodrigues.databases.DatabaseFactory;

import static com.jonasrodrigues.databases.DatabaseType.POSTGRE_SQL;

public class Main {

    private DatabaseFactory databaseFactory;

    public static void main(String[] args) {
        System.out.println("Example of use pattern FACTORY METHOD: ");
        Database database = new DatabaseFactory().getDatabase(); // get default database;
        database.connect();
        System.out.println(database.find(1));
        System.out.println(database.update(1, "name"));

        database = new DatabaseFactory().getDatabase(POSTGRE_SQL); // is possible load by env;
        System.out.println(database.find(1));
        System.out.println(database.update(1, "name"));
    }
}