package com.jonasrodrigues.databases;

final class Oracle implements Database {

    private String fakeDriver;

    protected Oracle(String driver) {
        this.fakeDriver = driver;
        connect();
    }

    private boolean isValidConnection() {
        return fakeDriver != null;
    }

    @Override
    public String find(Integer id) {
        if (!isValidConnection()) return "Error in database connection";
        return fakeDriver.concat("returned: " + id);
    }

    @Override
    public String update(Integer id, String query) {
        if (!isValidConnection()) return "Error in database connection";
        return fakeDriver.concat("updated id: " + id);
    }

    @Override
    public boolean connect() {
        this.fakeDriver.concat("connected: True");
        return true;
    }
}
