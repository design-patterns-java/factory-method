package com.jonasrodrigues.databases;

final class PostgreSql implements Database {

    private StringBuilder fakeMongoDriver;

    protected PostgreSql(StringBuilder driver) {
        this.fakeMongoDriver = driver;
        connect();
    }

    private boolean isValidConnection() {
        return fakeMongoDriver != null;
    }

    @Override
    public String find(Integer id) {
        if (!isValidConnection()) return "Error in database connection";
        return fakeMongoDriver.append("findById: ").append(id).toString();
    }

    @Override
    public String update(Integer id, String query) {
        if (!isValidConnection()) return "Error in database connection";
        return fakeMongoDriver.append("updateById: ")
                .append(id)
                .append("query: ")
                .append(query)
                .toString();
    }

    @Override
    public boolean connect() {
        this.fakeMongoDriver.append("Connected: True");
        return true;
    }
}
