package com.jonasrodrigues.databases;

import static com.jonasrodrigues.databases.DatabaseType.*;

public class DatabaseFactory {

    public Database getDatabase() {
        return getDatabase(ORACLE);
    }

    public Database getDatabase(DatabaseType type) {
        if (ORACLE.equals(type)) {
            return getOracle();
        }

        if (POSTGRE_SQL.equals(type)) {
            return getMongoDb();
        }
        throw new IllegalArgumentException("this driver is not implemented");
    }

    private Database getMongoDb() {
            StringBuilder driver = new StringBuilder();
            return new PostgreSql(driver);
    }

    private Database getOracle() {
        String driver = new String();
        return new Oracle(driver);
    }

}
