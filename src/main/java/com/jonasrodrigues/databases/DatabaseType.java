package com.jonasrodrigues.databases;

public enum DatabaseType {
    POSTGRE_SQL("PostgreSql"),
    ORACLE("Oracle");
    public final String driverClass;

    DatabaseType(String driverClass) {
        this.driverClass = driverClass;
    }
}
