package com.jonasrodrigues.databases;

public interface Database {

    String find(Integer id);

    String update(Integer id, String query);

    boolean connect();
}
